MP3 Lint
========

A linter for my MP3 collection.

Install
-------

Download and run this to install:

    pip install . --user --upgrade

Test
----

    tox

License
-------

ISC
